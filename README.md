Laravel GusApi
================

[SoapClient](http://php.net/manual/en/class.soapclient.php) integration for Laravel.

General System Requirements
-------------
- [PHP >7.1.0](http://php.net/)
- [Laravel ~5.6.*](https://github.com/laravel/framework)
- [Laravel SoapClient >1.0.0](https://bitbucket.org/rudashi/laravelsoap)
- [SAM-core ~1.*](https://bitbucket.org/rudashi/samcore)

Quick Installation
-------------
If needed use composer to grab the library

```
$ composer require rudashi/gus-api
```

Usage
-------------

###Dependency Injection

```php
public function __construct(\Rudashi\GusApi\GusApi $gusApi)
{
    $this->gusApi = $gusApi;
}
```

###Facade

```php
$gusApi = GusApi::login();
```

or directly

```php
$gusApi = GusApi::instance();
```

###Examples

```php
$this->gusApi->login();

$this->gusApi->getByKrs($krs);
$this->gusApi->getByKrsy([$krs1, $krs2, $krs3]);
$this->gusApi->getByNip($nip);
$this->gusApi->getByNipy([$nip1, $nip2, $nip3]);
$this->gusApi->getByRegon($regon);
$this->gusApi->getByRegony([$regon1, $regon2, $regon3]);
$this->gusApi->getByRegony14zn([$regonLong1, $regonLong2, $regonLong3]);
$this->gusApi->getFullReport($regon);

$this->gusApi->logout();
```

###API

Find information about company by: 
```
GET /gus/company?nip=
GET /gus/company?krs=
GET /gus/company?regon=
```

Authors
-------------

* **Borys Zmuda** - Lead designer - [GoldenLine](http://www.goldenline.pl/borys-zmuda/)