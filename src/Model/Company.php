<?php

namespace Rudashi\GusApi\Model;

use Rudashi\GusApi\Response\Contracts\CompanyResponseInterface;

class Company
{
    public const TYPE_COMPANY       = 'P';
    public const TYPE_PERSON        = 'F';
    public const TYPE_LOCAL_COMPANY = 'LP';
    public const TYPE_LOCAL__PERSON = 'LF';

    public $name;
    public $regon;
    public $nip;
    public $krs;
    public $province;
    public $district;
    public $commune;
    public $city;
    public $post_code;
    public $street;
    public $street_number;
    public $place_number;
    public $country_code;
    public $country;
    public $phone_number;
    public $email;
    public $website;


    public function __construct(CompanyResponseInterface $companyResponse = null)
    {
        if ($companyResponse) {
            $this->set($companyResponse);
        }
    }

    public function set(CompanyResponseInterface $companyResponse) : self
    {
        $this->name             = $companyResponse->getNazwa();
        $this->regon            = $companyResponse->getRegon();
        $this->nip              = $companyResponse->getNip();
        $this->krs              = $companyResponse->getKrs();
        $this->province         = $this->firstLetter($companyResponse->getWojewodztwo());
        $this->district         = $companyResponse->getPowiat();
        $this->commune          = $companyResponse->getGmina();
        $this->city             = $companyResponse->getMiejscowosc();
        $this->post_code        = $this->convertPostCode($companyResponse->getKodPocztowy());
        $this->street           = $companyResponse->getUlica();
        $this->street_number    = trim($companyResponse->getNumerNieruchomosci());
        $this->place_number     = trim($companyResponse->getNumerLokalu());
        $this->country_code     = $companyResponse->getKrajSymbol();
        $this->country          = $this->firstLetter($companyResponse->getKrajNazwa());
        $this->phone_number     = $companyResponse->getTelefon();
        $this->email            = strtolower($companyResponse->getEmail());
        $this->website          = $companyResponse->getWebsite();

        return $this;
    }

    public function getRegon() : string
    {
        return $this->regon;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getProvince() : string
    {
        return $this->province;
    }

    public function getDistrict() : string
    {
        return $this->district;
    }

    public function getCommune() : string
    {
        return $this->commune;
    }

    public function getCity() : string
    {
        return $this->city;
    }

    public function getPostCode() : string
    {
        return $this->post_code;
    }

    public function getStreet() : string
    {
        return $this->street;
    }

    private function convertPostCode(string $postCode) : string
    {
        if (strpos($postCode, '-') === false) {
            return substr_replace($postCode, '-', 2, 0);
        }
        return $postCode;
    }

    private function firstLetter(string $string = null) : string
    {
        if ($string === null) {
            return '';
        }
        return ucfirst(strtolower($string));
    }
}