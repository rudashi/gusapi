<?php

namespace Rudashi\GusApi;

use \Rudashi\LaravelSoap\Client\SoapClient as SoapClientMaster;

class SoapClient extends SoapClientMaster
{

    public function __doRequest($request, $location, $action, $version, $one_way = 0) : string
    {
        $response = parent::__doRequest($request, $location, $action, $version, $one_way);

        return stristr(stristr($response, '<s:'), '</s:Envelope>', true).'</s:Envelope>';
    }

}