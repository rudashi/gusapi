<?php

namespace Rudashi\GusApi\Environment;

use Rudashi\GusApi\Environment\Contracts\EnvironmentInterface;

class Production implements EnvironmentInterface
{

    public function serviceUrl() : string
    {
        return 'https://wyszukiwarkaregon.stat.gov.pl/wsBIR/UslugaBIRzewnPubl.svc';
    }

    public function wsdlUrl() : string
    {
        return 'https://wyszukiwarkaregontest.stat.gov.pl/wsBIR/wsdl/UslugaBIRzewnPubl.xsd';
    }
}