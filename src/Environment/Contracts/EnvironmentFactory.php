<?php

namespace Rudashi\GusApi\Environment\Contracts;

use Rudashi\GusApi\Environment\Development;
use Rudashi\GusApi\Environment\Production;
use Rudashi\GusApi\Exceptions\InvalidEnvironmentException;

class EnvironmentFactory
{

    public function create(string $environment) : EnvironmentInterface
    {
        return self::set($environment);
    }

    public static function set(string $environment) : EnvironmentInterface
    {
        switch ($environment) {
            case 'dev' :
            case 'development' :
            case 'local' :
                return new Development();
            case 'prod' :
            case 'production' :
                return new Production();
            default :
                throw new InvalidEnvironmentException("Invalid environment: {$environment}");
        }
    }
}