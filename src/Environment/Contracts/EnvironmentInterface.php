<?php

namespace Rudashi\GusApi\Environment\Contracts;

interface EnvironmentInterface
{
    public function serviceUrl() : string;

    public function wsdlUrl() : string;
}