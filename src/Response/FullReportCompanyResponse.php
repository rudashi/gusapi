<?php

namespace Rudashi\GusApi\Response;

use Rudashi\GusApi\Response\Contracts\CompanyResponseInterface;

class FullReportCompanyResponse implements CompanyResponseInterface
{

    protected $praw_nazwa;
    protected $praw_regon14;
    protected $praw_nip;
    protected $praw_numerWrejestrzeEwidencji;
    protected $praw_adSiedzWojewodztwo_Nazwa;
    protected $praw_adSiedzPowiat_Nazwa;
    protected $praw_adSiedzGmina_Nazwa;
    protected $praw_adSiedzMiejscowosc_Nazwa;
    protected $praw_adSiedzKodPocztowy;
    protected $praw_adSiedzUlica_Nazwa;
    protected $praw_adSiedzNumerNieruchomosci;
    protected $praw_adSiedzNumerLokalu;
    protected $praw_adSiedzKraj_Symbol;
    protected $praw_adSiedzKraj_Nazwa;
    protected $praw_numerTelefonu;
    protected $praw_adresEmail;
    protected $praw_adresStronyinternetowej;

    public function __construct(array $properties = [])
    {
        $this->setProperties($properties);
    }

    public function setProperties(array $properties) : self
    {
        foreach ($properties as $property => $value) {
            if (property_exists($this, $property)) {
                $this->{$property} = $value;
            }
        }

        return $this;
    }

    public function getNazwa(): string
    {
        return $this->praw_nazwa;
    }

    public function getRegon(): string
    {
        return $this->praw_regon14;
    }

    public function getNip(): ?string
    {
        return $this->praw_nip;
    }

    public function getKrs(): ?string
    {
        return $this->praw_numerWrejestrzeEwidencji;
    }

    public function getWojewodztwo(): string
    {
        return $this->praw_adSiedzWojewodztwo_Nazwa;
    }

    public function getPowiat(): string
    {
        return $this->praw_adSiedzPowiat_Nazwa;
    }

    public function getGmina(): string
    {
        return $this->praw_adSiedzGmina_Nazwa;
    }

    public function getMiejscowosc(): string
    {
        return $this->praw_adSiedzMiejscowosc_Nazwa;
    }

    public function getKodPocztowy(): string
    {
        return $this->praw_adSiedzKodPocztowy;
    }

    public function getUlica(): ?string
    {
        return $this->praw_adSiedzUlica_Nazwa;
    }

    public function getNumerNieruchomosci(): ?string
    {
        return $this->praw_adSiedzNumerNieruchomosci;
    }

    public function getNumerLokalu(): ?string
    {
        return $this->praw_adSiedzNumerLokalu;
    }

    public function getKrajSymbol(): ?string
    {
        return $this->praw_adSiedzKraj_Symbol;
    }

    public function getKrajNazwa(): ?string
    {
        return $this->praw_adSiedzKraj_Nazwa;
    }

    public function getTelefon(): ?string
    {
        return $this->praw_numerTelefonu;
    }

    public function getEmail(): ?string
    {
        return $this->praw_adresEmail;
    }

    public function getWebsite(): ?string
    {
        return $this->praw_adresStronyinternetowej;
    }

}