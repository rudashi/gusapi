<?php

namespace Rudashi\GusApi\Response;

use Rudashi\GusApi\Response\Contracts\FullReportPramTypes;
use Rudashi\GusApi\Response\Contracts\ResponseInterface;
use Rudashi\GusApi\Response\Contracts\CompanyResponseInterface;
use Rudashi\GusApi\Exceptions\InvalidParameterException;

class FullReportResponse implements ResponseInterface
{
    protected $DanePobierzPelnyRaportResult;

    private $type;

    public function setType(string $type) : self
    {
        $this->type = $type;

        return $this;
    }

    public function getResult() : self
    {
        if ($this->getDanePobierzPelnyRaportResult() instanceof CompanyResponseInterface) {
            return $this;
        }

        $xml = new \SimpleXMLElement($this->getDanePobierzPelnyRaportResult());

        $this->setDanePobierzPelnyRaportResult($this->setResponseReport()->setProperties( (array) $xml->children()->children() ));

        return $this;
    }

    public function setDanePobierzPelnyRaportResult($DanePobierzPelnyRaportResult) : self
    {
        $this->DanePobierzPelnyRaportResult = $DanePobierzPelnyRaportResult;

        return $this;
    }

    public function isResult() : bool
    {
        return $this->DanePobierzPelnyRaportResult !== '';
    }

    public function getDanePobierzPelnyRaportResult()
    {
        return $this->DanePobierzPelnyRaportResult;
    }

    public function setResponseReport()
    {
        switch($this->type) {
            case FullReportPramTypes::COMPANY :
                return new FullReportCompanyResponse();
            case FullReportPramTypes::PERSON :
                return new FullReportPersonResponse();
            default :
                throw new InvalidParameterException("Invalid type parameter. Given: {$this->type}.");
        }
    }

}