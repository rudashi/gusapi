<?php

namespace Rudashi\GusApi\Response;

use Rudashi\GusApi\Response\Contracts\CompanyResponseInterface;

class FullReportPersonResponse implements CompanyResponseInterface
{

    protected $fiz_nazwa;
    protected $fiz_regon9;
    protected $fiz_nip;
    protected $fizC_numerwRejestrzeEwidencji;
    protected $fiz_adSiedzWojewodztwo_Nazwa;
    protected $fiz_adSiedzPowiat_Nazwa;
    protected $fiz_adSiedzGmina_Nazwa;
    protected $fiz_adSiedzMiejscowosc_Nazwa;
    protected $fiz_adSiedzKodPocztowy;
    protected $fiz_adSiedzUlica_Nazwa;
    protected $fiz_adSiedzNumerNieruchomosci;
    protected $fiz_adSiedzNumerLokalu;
    protected $fiz_adSiedzKraj_Symbol;
    protected $fiz_adSiedzKraj_Nazwa;
    protected $fiz_numerTelefonu;
    protected $fiz_adresEmail;
    protected $fiz_adresStronyinternetowej;

    public function __construct(array $properties = [])
    {
        $this->setProperties($properties);
    }

    public function setProperties(array $properties) : self
    {
        foreach ($properties as $property => $value) {
            if (property_exists($this, $property)) {
                $this->{$property} = $value;
            }
        }

        return $this;
    }

    public function getNazwa(): string
    {
        return $this->fiz_nazwa;
    }

    public function getRegon(): string
    {
        return $this->fiz_regon9;
    }

    public function getNip(): ?string
    {
        return null;
    }

    public function getKrs(): ?string
    {
        return null;
    }

    public function getWojewodztwo(): string
    {
        return $this->fiz_adSiedzWojewodztwo_Nazwa;
    }

    public function getPowiat(): string
    {
        return $this->fiz_adSiedzPowiat_Nazwa;
    }

    public function getGmina(): string
    {
        return $this->fiz_adSiedzGmina_Nazwa;
    }

    public function getMiejscowosc(): string
    {
        return $this->fiz_adSiedzMiejscowosc_Nazwa;
    }

    public function getKodPocztowy(): string
    {
        return $this->fiz_adSiedzKodPocztowy;
    }

    public function getUlica(): ?string
    {
        return $this->fiz_adSiedzUlica_Nazwa;
    }

    public function getNumerNieruchomosci(): ?string
    {
        return $this->fiz_adSiedzNumerNieruchomosci;
    }

    public function getNumerLokalu(): ?string
    {
        return $this->fiz_adSiedzNumerLokalu;
    }

    public function getKrajSymbol(): ?string
    {
        return $this->fiz_adSiedzKraj_Symbol;
    }

    public function getKrajNazwa(): ?string
    {
        return $this->fiz_adSiedzKraj_Nazwa;
    }

    public function getTelefon(): ?string
    {
        return $this->fiz_numerTelefonu;
    }

    public function getEmail(): ?string
    {
        return $this->fiz_adresEmail;
    }

    public function getWebsite(): ?string
    {
        return $this->fiz_adresStronyinternetowej;
    }

}