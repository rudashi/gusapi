<?php

namespace Rudashi\GusApi\Response;

use Rudashi\GusApi\Response\Contracts\ResponseInterface;

class LogoutResponse implements ResponseInterface
{

    protected $WylogujResult;

    public function __construct(string $sessionId)
    {
        $this->WylogujResult = $sessionId;
    }

    public function getResult() : bool
    {
        return $this->WylogujResult;
    }

}