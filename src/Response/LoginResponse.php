<?php

namespace Rudashi\GusApi\Response;

use Rudashi\GusApi\Response\Contracts\ResponseInterface;

class LoginResponse implements ResponseInterface
{

    protected $ZalogujResult;

    public function __construct(string $sessionId)
    {
        $this->ZalogujResult = $sessionId;
    }

    public function getResult() : string
    {
        return $this->ZalogujResult;
    }

    public function isAuthorized() : bool
    {
        return $this->ZalogujResult !== '';
    }

}