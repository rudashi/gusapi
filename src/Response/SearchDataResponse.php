<?php

namespace Rudashi\GusApi\Response;

use Rudashi\GusApi\Response\Contracts\ResponseInterface;

class SearchDataResponse implements ResponseInterface
{
    /**
     * @var []|DataSearchCompanyResponse|DataSearchCompanyResponse[]
     */
    protected $DaneSzukajResult;

    public function getResult() : self
    {
        if (\is_array($this->getDaneSzukajResult()) || $this->getDaneSzukajResult() instanceof DataSearchCompanyResponse) {
            return $this;
        }

        $xml = new \SimpleXMLElement($this->getDaneSzukajResult());

        if ($xml->children()->count() === 1) {
            $this->setDaneSzukajResult(new DataSearchCompanyResponse((array) $xml->children()->children()));
        } else {
            $response = [];

            foreach ($xml->children() as $element) {
                $response[]  = new DataSearchCompanyResponse( (array) $element );
            }

            $this->setDaneSzukajResult($response);
        }

        return $this;
    }

    public function setDaneSzukajResult($DaneSzukajResult) : self
    {
        $this->DaneSzukajResult = $DaneSzukajResult;

        return $this;
    }

    public function isResult() : bool
    {
        return $this->DaneSzukajResult !== '';
    }

    /**
     * @return mixed
     */
    public function getDaneSzukajResult()
    {
        return $this->DaneSzukajResult;
    }

    public function isSingleResponse() : bool
    {
        return $this->DaneSzukajResult instanceof DataSearchCompanyResponse;
    }

}