<?php

namespace Rudashi\GusApi\Response;

use Rudashi\GusApi\Response\Contracts\CompanyResponseInterface;

class DataSearchCompanyResponse implements CompanyResponseInterface
{

    public $Regon;

    public $RegonLink;

    public $Nazwa;

    public $Wojewodztwo;

    public $Powiat;

    public $Gmina;

    public $Miejscowosc;

    public $KodPocztowy;

    public $Ulica;

    public $Typ;

    public $SilosID;

    public function __construct(array $properties)
    {
        $this->setProperties($properties);
    }

    public function setProperties(array $properties) : self
    {
        foreach ($properties as $property => $value) {
            if (property_exists($this, $property)) {
                $this->{$property} = $value;
            }
        }

        return $this;
    }

    public function getRegon() : string
    {
        return $this->Regon;
    }

    public function getRegonLink() : string
    {
        return $this->RegonLink;
    }

    public function getNazwa() : string
    {
        return $this->Nazwa;
    }

    public function getWojewodztwo() : string
    {
        return $this->Wojewodztwo;
    }

    public function getPowiat() : string
    {
        return $this->Powiat;
    }

    public function getGmina() : string
    {
        return $this->Gmina;
    }

    public function getMiejscowosc() : string
    {
        return $this->Miejscowosc;
    }

    public function getKodPocztowy() : string
    {
        return $this->KodPocztowy;
    }

    public function getUlica() : ?string
    {
        return $this->Ulica;
    }

    public function getTyp() : string
    {
        return $this->Typ;
    }

    public function getSilosID() : int
    {
        return $this->SilosID;
    }

    public function getNip(): ?string
    {
        return null;
    }

    public function getKrs(): ?string
    {
        return null;
    }

    public function getNumerNieruchomosci(): ?string
    {
        return null;
    }

    public function getNumerLokalu(): ?string
    {
        return null;
    }

    public function getKrajSymbol(): ?string
    {
        return null;
    }

    public function getKrajNazwa(): ?string
    {
        return null;
    }

    public function getTelefon(): ?string
    {
        return null;
    }

    public function getEmail(): ?string
    {
        return null;
    }

    public function getWebsite(): ?string
    {
        return null;
    }

}