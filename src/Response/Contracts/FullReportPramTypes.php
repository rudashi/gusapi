<?php

namespace Rudashi\GusApi\Response\Contracts;

class FullReportPramTypes
{
    public const COMPANY = 'PublDaneRaportPrawna';
    public const PERSON  = 'PublDaneRaportDzialalnoscFizycznejCeidg';
}