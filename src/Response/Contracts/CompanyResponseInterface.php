<?php

namespace Rudashi\GusApi\Response\Contracts;

interface CompanyResponseInterface
{

    public function setProperties(array $properties);

    public function getNazwa() : string;
    
    public function getRegon() : string;

    public function getNip() : ?string;

    public function getKrs() : ?string;

    public function getWojewodztwo() : string;
    
    public function getPowiat() : string;
    
    public function getGmina() : string;
    
    public function getMiejscowosc() : string;
    
    public function getKodPocztowy() : string;
    
    public function getUlica() : ?string;

    public function getNumerNieruchomosci() : ?string;

    public function getNumerLokalu() : ?string;

    public function getKrajSymbol() : ?string;

    public function getKrajNazwa() : ?string;

    public function getTelefon() : ?string;

    public function getEmail() : ?string;

    public function getWebsite() : ?string;

}