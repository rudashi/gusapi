<?php

namespace Rudashi\GusApi\Response\Contracts;

interface ResponseInterface
{

    public function getResult();

}