<?php

namespace Rudashi\GusApi;

use Totem\SamCore\App\Resources\Api;

class Controller extends \Illuminate\Routing\Controller
{

    public function company(\Illuminate\Http\Request $request) : Api
    {
        return new Api(
            (new \Rudashi\GusApi\Api())->getBy($request)
        );
    }
}