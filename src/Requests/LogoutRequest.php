<?php

namespace Rudashi\GusApi\Requests;

class LogoutRequest
{
    protected $pIdentyfikatorSesji;

    public function __construct(string $sessionId)
    {
        $this->pIdentyfikatorSesji = $sessionId;
    }

    public function getPIdentyfikatorSesji() : string
    {
        return $this->pIdentyfikatorSesji;
    }
}