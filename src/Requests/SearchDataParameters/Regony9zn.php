<?php

namespace Rudashi\GusApi\Requests\SearchDataParameters;

use Rudashi\GusApi\Exceptions\InvalidParameterException;
use Rudashi\GusApi\Requests\SearchDataParameters\Contracts\SearchDataParameterInterface;

class Regony9zn implements SearchDataParameterInterface
{

    protected $Regony9zn;

    public function set($parameter) : self
    {
        $this->checkParameter(explode(',' ,$parameter));

        $this->Regony9zn = $parameter;

        return $this;
    }

    public function checkParameter(array $parameters) : void
    {
        foreach($parameters as $parameter) {
            $length = \strlen($parameter);
            if ($length !== 9) {
                throw new InvalidParameterException("Invalid parameter. Given {$length} should be 9.");
            }
        }
    }
}