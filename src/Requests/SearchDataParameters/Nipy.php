<?php

namespace Rudashi\GusApi\Requests\SearchDataParameters;

use Rudashi\GusApi\Requests\SearchDataParameters\Contracts\SearchDataParameterInterface;

class Nipy implements SearchDataParameterInterface
{

    protected $Nipy;

    public function set($parameter) : self
    {
        $this->Nipy = $parameter;

        return $this;
    }
}