<?php

namespace Rudashi\GusApi\Requests\SearchDataParameters;

use Rudashi\GusApi\Requests\SearchDataParameters\Contracts\SearchDataParameterInterface;

class Regon implements SearchDataParameterInterface
{

    protected $Regon;

    public function set($parameter) : self
    {
        $this->Regon = $parameter;

        return $this;
    }
}