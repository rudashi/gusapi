<?php

namespace Rudashi\GusApi\Requests\SearchDataParameters;

use Rudashi\GusApi\Requests\SearchDataParameters\Contracts\SearchDataParameterInterface;

class Krsy implements SearchDataParameterInterface
{

    protected $Krsy;

    public function set($parameter) : self
    {
        $this->Krsy = $parameter;

        return $this;
    }
}