<?php

namespace Rudashi\GusApi\Requests\SearchDataParameters\Contracts;

use Rudashi\GusApi\Requests\SearchDataParameters\Krs;
use Rudashi\GusApi\Requests\SearchDataParameters\Krsy;
use Rudashi\GusApi\Requests\SearchDataParameters\Nip;
use Rudashi\GusApi\Requests\SearchDataParameters\Nipy;
use Rudashi\GusApi\Requests\SearchDataParameters\Regon;
use Rudashi\GusApi\Requests\SearchDataParameters\Regony14zn;
use Rudashi\GusApi\Requests\SearchDataParameters\Regony9zn;
use Rudashi\GusApi\Exceptions\InvalidSearchDataParameterException;

class SearchDataParamFactory
{

    public function create(string $parameter) : SearchDataParameterInterface
    {
        return self::set($parameter);
    }

    public static function set(string $parameter) : SearchDataParameterInterface
    {
        switch ($parameter) {
            case SearchDataPramTypes::KRS :
                return new Krs();
            case SearchDataPramTypes::KRSES :
                return new Krsy();
            case SearchDataPramTypes::NIP :
                return new Nip();
            case SearchDataPramTypes::NIPS :
                return new Nipy();
            case SearchDataPramTypes::REGON :
                return new Regon();
            case SearchDataPramTypes::REGONS_9 :
                return new Regony9zn();
            case SearchDataPramTypes::REGONS_14 :
                return new Regony14zn();
            default :
                throw new InvalidSearchDataParameterException("Invalid environment: {$parameter}");
        }
    }
}