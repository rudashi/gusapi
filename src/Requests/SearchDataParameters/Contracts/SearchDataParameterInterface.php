<?php

namespace Rudashi\GusApi\Requests\SearchDataParameters\Contracts;

interface SearchDataParameterInterface
{
    public function set($parameter) ;
}