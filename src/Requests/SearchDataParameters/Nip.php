<?php

namespace Rudashi\GusApi\Requests\SearchDataParameters;

use Rudashi\GusApi\Requests\SearchDataParameters\Contracts\SearchDataParameterInterface;

class Nip implements SearchDataParameterInterface
{

    protected $Nip;

    public function set($parameter) : Nip
    {
        $this->Nip = $parameter;

        return $this;
    }
}