<?php

namespace Rudashi\GusApi\Requests\SearchDataParameters;

use Rudashi\GusApi\Requests\SearchDataParameters\Contracts\SearchDataParameterInterface;

class Krs implements SearchDataParameterInterface
{

    protected $Krs;

    public function set($parameter) : self
    {
        $this->Krs = $parameter;

        return $this;
    }
}