<?php

namespace Rudashi\GusApi\Requests;

use Rudashi\GusApi\Requests\SearchDataParameters\Contracts\SearchDataParameterInterface;

class SearchDataRequest
{

    protected $pParametryWyszukiwania;

    public function __construct(SearchDataParameterInterface $dataParameter)
    {
        $this->pParametryWyszukiwania = $dataParameter;
    }

    public function getParametryWyszukiwania() : SearchDataParameterInterface
    {
        return $this->pParametryWyszukiwania;
    }

}