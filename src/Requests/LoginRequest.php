<?php

namespace Rudashi\GusApi\Requests;

class LoginRequest
{

    protected $pKluczUzytkownika;

    public function __construct(string $userKey)
    {
        $this->pKluczUzytkownika = $userKey;
    }

    public function getPKluczUzytkownika() : string
    {
        return $this->pKluczUzytkownika;
    }

}