<?php

namespace Rudashi\GusApi\Requests;

class FullRaportRequest
{

    protected $pRegon;
    protected $pNazwaRaportu;

    public function __construct(string $regon, $name)
    {
        $this->pRegon           = $regon;
        $this->pNazwaRaportu    = $name;
    }

    public function getPRegon() : string
    {
        return $this->pRegon;
    }

    public function getPNazwaRaportu() : string
    {
        return $this->pNazwaRaportu;
    }

}