<?php

namespace Rudashi\GusApi;

use Illuminate\Support\Facades\Facade;

class GusApiFacade extends Facade
{

    protected static function getFacadeAccessor() : string
    {
        return GusApi::class;
    }
}