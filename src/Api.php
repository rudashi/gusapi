<?php

namespace Rudashi\GusApi;

use Illuminate\Http\Request;
use Totem\SamCore\App\Traits\HasApi;
use Totem\SamCore\App\Traits\FilterableTrait;

class Api extends GusApi
{

    use FilterableTrait, HasApi;


    public function __construct(string $userKey = '', string $environment = 'prod')
    {
        parent::__construct(config('gus-api.userKey'), config('gus-api.environment'));

        $this->filterable = [
            'nip'   => 'getFullReportByNip',
            'krs'   => 'getFullReportByKrs',
            'regon' => 'getFullReportByRegon',
        ];
        $this->login();
    }

    public function getBy(Request $request)
    {
        /** @var \Rudashi\GusApi\Response\FullReportResponse $result */
        try {
            $result = $this->applyRequestFilter($request);
        } catch (\Exception $exception) {
            return $this->error(400, $exception->getMessage());
        }

        return $this->success($result->getDanePobierzPelnyRaportResult());
    }
}