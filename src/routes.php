<?php

Route::group(['prefix' => 'api'], static function() {

    Route::group(['prefix' => 'gus'], static function(){

        Route::get('company', 'Rudashi\GusApi\Controller@company')->name('api.gus.company');
    });
});