<?php

namespace Rudashi\GusApi;

use Rudashi\GusApi\Exceptions\ResultNotFoundException;
use Rudashi\GusApi\Model\Company;
use Rudashi\GusApi\Requests\LoginRequest;
use Rudashi\GusApi\Requests\LogoutRequest;
use Rudashi\GusApi\Requests\SearchDataRequest;
use Rudashi\GusApi\Requests\FullRaportRequest;
use Rudashi\GusApi\Response\Contracts\FullReportPramTypes;
use Rudashi\GusApi\Response\FullReportResponse;
use Rudashi\GusApi\Response\SearchDataResponse;
use Rudashi\GusApi\Response\DataSearchCompanyResponse;
use Rudashi\GusApi\Requests\SearchDataParameters\Contracts\SearchDataPramTypes;
use Rudashi\GusApi\Requests\SearchDataParameters\Contracts\SearchDataParamFactory;
use Rudashi\GusApi\Requests\SearchDataParameters\Contracts\SearchDataParameterInterface;
use Rudashi\GusApi\Exceptions\InvalidUserKeyException;
use Rudashi\GusApi\Exceptions\SearchLimitExceededException;

class GusApi
{
    /**
     * @var string
     */
    private $userKey;

    /**
     * @var GusApiClient
     */
    protected $client;

    /**
     * @var string
     */
    protected $sessionId;

    private const LIMIT_SEARCH = 10;

    public function __construct(string $userKey = '', string $environment = 'prod')
    {
        $this->setUserKey($userKey);
        $this->setClient(new GusApiClient($environment));
    }

    public function instance() : GusApi
    {
        return $this;
    }

    public function setUserKey(string $userKey) : GusApi
    {
        $this->userKey = $userKey;

        return $this;
    }

    public function setClient($client) : GusApi
    {
        $this->client = $client;

        return $this;
    }

    public function setSessionId($sessionId) : GusApi
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    public function getSessionId() : string
    {
        return $this->sessionId;
    }

    public function getClient() : GusApiClient
    {
        return $this->client;
    }

    public function getUserKey() : string
    {
        return $this->userKey;
    }

    public function login() : GusApi
    {
        $response = $this->client->login(new LoginRequest($this->getUserKey()));

        if ($response->isAuthorized() === false) {
            throw new InvalidUserKeyException("User key {$this->getUserKey()} is invalid");
        }

        $this->setSessionId($response->getResult());

        return $this;
    }

    public function logout() : bool
    {
        $response = $this->client->logout(new LogoutRequest($this->getSessionId()));

        return $response->getResult();
    }

    public function getByKrs(string $value) : SearchDataResponse
    {
        return $this->search(SearchDataParamFactory::set(SearchDataPramTypes::KRS), $value);
    }

    public function getByKrsy(array $values) : SearchDataResponse
    {
        $this->checkLimit($values);

        return $this->search(SearchDataParamFactory::set(SearchDataPramTypes::KRSES), implode(',', $values));
    }

    public function getByNip(string $value) : SearchDataResponse
    {
        return $this->search(SearchDataParamFactory::set(SearchDataPramTypes::NIP), $value);
    }

    public function getByNipy(array $values) : SearchDataResponse
    {
        $this->checkLimit($values);

        return $this->search(SearchDataParamFactory::set(SearchDataPramTypes::NIPS), implode(',', $values));
    }

    public function getByRegon(string $value) : SearchDataResponse
    {
        return $this->search(SearchDataParamFactory::set(SearchDataPramTypes::REGON), $value);
    }

    public function getByRegony(array $values) : SearchDataResponse
    {
        $this->checkLimit($values);

        return $this->search(SearchDataParamFactory::set(SearchDataPramTypes::REGONS_9), implode(',', $values));
    }

    public function getByRegony14zn(array $values) : SearchDataResponse
    {
        $this->checkLimit($values);

        return $this->search(SearchDataParamFactory::set(SearchDataPramTypes::REGONS_14), implode(',', $values));
    }

    public function search(SearchDataParameterInterface $typeParameter, string $parameters) : SearchDataResponse
    {
        $result = $this->client->searchData(
            new SearchDataRequest(
                $typeParameter->set($parameters)
            )
        );

        if ($result->isSingleResponse()) {
            return $result->setDaneSzukajResult(new Company($result->getDaneSzukajResult()));
        }
        return $result->setDaneSzukajResult(array_map(static function(DataSearchCompanyResponse $company) {
            return new Company($company);
        }, $result->getDaneSzukajResult()));
    }

    public function getFullReport(string $regon) : FullReportResponse
    {
        try {
            $result = $this->client->getFullReport(
                new FullRaportRequest($regon, FullReportPramTypes::COMPANY),
                FullReportPramTypes::COMPANY
            );
        } catch (ResultNotFoundException $e) {
            $result = $this->client->getFullReport(
                new FullRaportRequest($regon, FullReportPramTypes::PERSON),
                FullReportPramTypes::PERSON
            );
        }
        return $result->setDanePobierzPelnyRaportResult(new Company($result->getDanePobierzPelnyRaportResult()));
    }

    public function getFullReportByRegon(string $regon) : FullReportResponse
    {
        return $this->getFullReport($regon);
    }

    public function getFullReportByNip(string $nip) : FullReportResponse
    {
        return $this->getFullReport($this->getByNip($nip)->getDaneSzukajResult()->getRegon());
    }

    public function getFullReportByKrs(string $krs) : FullReportResponse
    {
        return $this->getFullReport($this->getByKrs($krs)->getDaneSzukajResult()->getRegon());
    }

    public function checkLimit(array $values) : void
    {
        if (\count($values) > self::LIMIT_SEARCH) {
            throw new SearchLimitExceededException('Too many identifiers. Maximum allowed is ' .self::LIMIT_SEARCH);
        }
    }
}