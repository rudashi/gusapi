<?php

namespace Rudashi\GusApi;

use Rudashi\GusApi\Exceptions\ResultNotFoundException;
use Rudashi\GusApi\Requests\FullRaportRequest;
use Rudashi\GusApi\Response\FullReportResponse;
use Rudashi\LaravelSoap\Client\Soap;
use Rudashi\LaravelSoap\Client\SoapService;
use Rudashi\LaravelSoap\Client\SoapTrait;
use Rudashi\GusApi\Requests\LoginRequest;
use Rudashi\GusApi\Requests\LogoutRequest;
use Rudashi\GusApi\Requests\SearchDataRequest;
use Rudashi\GusApi\Response\SearchDataResponse;
use Rudashi\GusApi\Response\LogoutResponse;
use Rudashi\GusApi\Response\LoginResponse;
use Rudashi\GusApi\Environment\Contracts\EnvironmentFactory;

class GusApiClient
{

    use SoapTrait;

    public const NAMESPACE = 'http://www.w3.org/2005/08/addressing';

    public const ACTION_PREFIX = 'http://CIS/BIR/PUBL/2014/07/IUslugaBIRzewnPubl/';

    public const SERVICE = 'GUS';

    protected $environment;

    /**
     * @var Soap
     */
    protected $soap;

    public function __construct(string $environment)
    {
        $this->environment = (new EnvironmentFactory())->create($environment);

        $this->build();
    }

    protected function build() : Soap
    {
        $this->soap = new Soap();
        $this->soap->addService(self::SERVICE, function(SoapService $service) {
            $service->setWsdl($this->environment->wsdlUrl())
                ->setLocation($this->environment->serviceUrl())
                ->setTrace(true)
                ->setClassMap([
                    'ZalogujResponse' => LoginResponse::class,
                    'WylogujResponse' => LogoutResponse::class,
                    'DaneSzukajResponse' => SearchDataResponse::class,
                    'DanePobierzPelnyRaportResponse' => FullReportResponse::class
                ])
                ->setHeader(self::NAMESPACE, 'To', $this->environment->serviceUrl())
                ->setClient(new SoapClient($service->getWsdl(), $service->getOptions(), $service->getHeaders()))
                ;
        });

        return $this->soap;
    }

    public function login(LoginRequest $request) : LoginResponse
    {
        $response = $this->call('Zaloguj', $request );

        if ($response->isAuthorized() === true) {
            $this->soap->editService(self::SERVICE, static function (SoapService $service) use ($response) {
                $service->extendsOptions([
                    'stream_context' => stream_context_create([
                        'http' => [
                            'header' => 'sid: ' . $response->getResult()
                        ]
                    ])
                ])
                ->setClient(new SoapClient($service->getWsdl(), $service->getOptions(), $service->getHeaders()))
                ;
            });
        }

        return $response;
    }

    public function logout(LogoutRequest $request) : LogoutResponse
    {
        return $this->call('Wyloguj', $request );
    }

    public function call(string $function, $data)
    {
        return $this->soap->call(self::SERVICE, $function, [$data], [], $this->setExtraHeader($function));
    }

    public function searchData(SearchDataRequest $request) : SearchDataResponse
    {
        /** @var SearchDataResponse $result */
        $result = $this->call('DaneSzukaj', $request);

        if ($result->isResult() === false) {
            throw new ResultNotFoundException('Nothing found');
        }

        return $result->getResult();
    }

    public function getFullReport(FullRaportRequest $request, string $reportType) : FullReportResponse
    {
        /** @var FullReportResponse $result */
        $result = $this->call('DanePobierzPelnyRaport', $request);

        if ($result->isResult() === false) {
            throw new ResultNotFoundException('Nothing found');
        }
        return $result->setType($reportType)->getResult();
    }

    protected function setExtraHeader(string $action) : array
    {
        return [
            $this->soap->extraHeader(self::NAMESPACE, 'Action', self::ACTION_PREFIX. $action),
        ];
    }

}