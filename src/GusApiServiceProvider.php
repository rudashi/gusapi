<?php

namespace Rudashi\GusApi;

use Illuminate\Support\ServiceProvider;

class GusApiServiceProvider extends ServiceProvider
{

    public function getNamespace() : string
    {
        return 'gus-api';
    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() : void
    {
        $this->publishes([
            __DIR__ . '/config/config.php' => config_path($this->getNamespace().'.php'),
        ], $this->getNamespace().'-config');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() : void
    {
        $this->mergeConfigFrom(__DIR__ . '/config/config.php', $this->getNamespace());
        $this->loadRoutesFrom(__DIR__.'/routes.php');

        $this->app->bindIf(GusApi::class, static function () {
            return new GusApi(config('gus-api.userKey'), config('gus-api.environment'));
        });
    }
}