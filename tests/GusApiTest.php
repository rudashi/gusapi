<?php

namespace GusApi\Tests;

use Rudashi\GusApi\Exceptions\InvalidParameterException;
use Rudashi\GusApi\GusApi;
use Rudashi\GusApi\GusApiClient;
use Rudashi\GusApi\Model\Company;
use Rudashi\GusApi\Response\FullReportResponse;
use Rudashi\GusApi\Response\SearchDataResponse;
use Rudashi\GusApi\Response\LogoutResponse;
use Rudashi\GusApi\Response\LoginResponse;
use Rudashi\GusApi\Requests\LoginRequest;
use Rudashi\GusApi\Requests\LogoutRequest;
use Rudashi\GusApi\Requests\SearchDataRequest;
use Rudashi\GusApi\Requests\SearchDataParameters\Contracts\SearchDataParamFactory;
use Rudashi\GusApi\Requests\SearchDataParameters\Contracts\SearchDataPramTypes;
use Rudashi\GusApi\Exceptions\InvalidUserKeyException;
use Rudashi\GusApi\Exceptions\ResultNotFoundException;
use PHPUnit\Framework\TestCase;

class GusApiTest extends TestCase
{

    protected $userKey = 'abcde12345abcde12345';

    /**
     * @var \PHPUnit\Framework\MockObject\MockObject|GusApiClient
     */
    protected $client;

    /**
     * @var GusApi
     */
    protected $api;

    public function setUp(): void
    {
        $this->client = $this->createMock(GusApiClient::class);
        $this->api = new GusApi($this->userKey, 'dev');
    }

    public function testInvalidLogin() : void
    {
        $this->expectException(InvalidUserKeyException::class);
        $this->login('invalid-user-key');
    }

    public function testLogout() : void
    {
        $this->login();

        $this->client
            ->method('logout')
            ->with(new LogoutRequest($this->api->getSessionId()))
            ->willReturn(new LogoutResponse(true));

        $this->assertTrue($this->api->logout());
    }

    public function testGetNip() : void
    {
        $this->login();

        $this->client
            ->method('searchData')
            ->with(new SearchDataRequest(SearchDataParamFactory::set($this->searchDataParameters('getByNip')['factory'])->set($this->searchDataParameters('getByNip')['value'])))
            ->willReturn(new SearchDataResponse());

        $result = $this->api->getByNip($this->searchDataParameters('getByNip')['value']);
        $this->assertValidCompanyMain($result->getDaneSzukajResult());
    }

    public function testDataSearch() : void
    {
        $this->login();

        foreach ($this->searchDataParameters() as $method => $type) {
            /** @var SearchDataResponse $result */
            $result = $this->api->{$method}($type['value']);

            $this->assertInstanceOf(SearchDataResponse::class, $result);

            if (\is_array($result->getDaneSzukajResult())) {
                $this->assertValidCompanyMain($result->getDaneSzukajResult()[0]);
                $this->assertValidCompanySecond($result->getDaneSzukajResult()[1]);
            } else {
                $this->assertValidCompanyMain($result->getDaneSzukajResult());
            }
        }
    }

    public function testGetInvalidKrs() : void
    {
        $this->login();
        $this->expectException(ResultNotFoundException::class);

        $this->api->getByKrs('PL'.$this->searchDataParameters('getByKrs')['value']);
    }

    public function testGetInvalidKrsy() : void
    {
        $this->login();
        $this->expectException(ResultNotFoundException::class);

        $this->api->getByKrsy(['0', '1']);
    }

    public function testGetInvalidNip() : void
    {
        $this->login();
        $this->expectException(ResultNotFoundException::class);

        $this->api->getByNip('PL'.$this->searchDataParameters('getByNip')['value']);
    }

    public function testGetInvalidNipy() : void
    {
        $this->login();
        $this->expectException(ResultNotFoundException::class);

        $this->api->getByNipy(['0', '1']);
    }

    public function testGetInvalidRegon() : void
    {
        $this->login();
        $this->expectException(ResultNotFoundException::class);

        $this->api->getByRegon(000);
    }

    public function testGetInvalidRegony() : void
    {
        $this->login();
        $this->expectException(ResultNotFoundException::class);

        $this->api->getByRegony(['000000000', '111111111']);
    }

    public function testGetInvalidRegony14zn() : void
    {
        $this->login();
        $this->expectException(ResultNotFoundException::class);

        $this->api->getByRegony14zn(['00000000000000', '11111111111111']);
    }

    public function testGetInvalidParameterRegony() : void
    {
        $this->login();
        $this->expectException(InvalidParameterException::class);

        $this->api->getByRegony(['abc']);
    }

    public function testGetInvalidParameterRegony14zn() : void
    {
        $this->login();
        $this->expectException(InvalidParameterException::class);

        $this->api->getByRegony14zn(['abc']);
    }

    public function testGetFullReport() : void
    {
        $this->login();

        $this->client
            ->method('getFullReport')
            ->with(new SearchDataRequest(SearchDataParamFactory::set($this->searchDataParameters('getByRegon')['factory'])->set($this->searchDataParameters('getByRegon')['value'])))
            ->willReturn(new FullReportResponse());

        $result = $this->api->getFullReport($this->searchDataParameters('getByRegon')['value']);
        $this->assertValidCompanyMain($result->getDanePobierzPelnyRaportResult());
    }

    public function testGetInvalidFulLReport() : void
    {
        $this->login();
        $this->expectException(ResultNotFoundException::class);

        $this->api->getFullReport('00000000000000');
    }

    public function testGetFullReportByNip() : void
    {
        $this->login();

        $this->client
            ->method('getFullReport')
            ->with(new SearchDataRequest(SearchDataParamFactory::set($this->searchDataParameters('getByNip')['factory'])->set($this->searchDataParameters('getByNip')['value'])))
            ->willReturn(new FullReportResponse());

        $result = $this->api->getFullReportByNip($this->searchDataParameters('getByNip')['value']);
        $this->assertValidCompanyMain($result->getDanePobierzPelnyRaportResult());
    }

    public function testGetFullReportByRegon() : void
    {
        $this->login();

        $this->client
            ->method('getFullReport')
            ->with(new SearchDataRequest(SearchDataParamFactory::set($this->searchDataParameters('getByRegon')['factory'])->set($this->searchDataParameters('getByRegon')['value'])))
            ->willReturn(new FullReportResponse());

        $result = $this->api->getFullReportByRegon($this->searchDataParameters('getByRegon')['value']);
        $this->assertValidCompanyMain($result->getDanePobierzPelnyRaportResult());
    }

    public function testGetFullReportByKrs() : void
    {
        $this->login();

        $this->client
            ->method('getFullReport')
            ->with(new SearchDataRequest(SearchDataParamFactory::set($this->searchDataParameters('getByKrs')['factory'])->set($this->searchDataParameters('getByKrs')['value'])))
            ->willReturn(new FullReportResponse());

        $result = $this->api->getFullReportByKrs($this->searchDataParameters('getByKrs')['value']);
        $this->assertValidCompanyMain($result->getDanePobierzPelnyRaportResult());
    }

    protected function login($userKey = null): GusApi
    {
        $this->client
            ->method('login')
            ->with(new LoginRequest($userKey ?: $this->userKey))
            ->willReturn(new LoginResponse(''));
        $this->api->setUserKey($userKey ?: $this->userKey);

        return $this->api->login();
    }

    protected function assertValidCompanyMain(Company $company) : void
    {
        $this->assertSame('09118782600000', $company->getRegon());
        $this->assertSame('"TOTEM.COM.PL SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ" SPÓŁKA KOMANDYTOWA', $company->getName());
        $this->assertSame('Kujawsko-pomorskie', $company->getProvince());
        $this->assertSame('inowrocławski', $company->getDistrict());
        $this->assertSame('Inowrocław', $company->getCommune());
        $this->assertSame('Inowrocław', $company->getCity());
        $this->assertSame('88-100', $company->getPostCode());
        $this->assertSame('ul. Test-Krucza', $company->getStreet());
    }

    protected function assertValidCompanySecond(Company $company) : void
    {
        $this->assertSame('63013996000000', $company->getRegon());
        $this->assertSame('ENEA SPÓŁKA AKCYJNA', $company->getName());
        $this->assertSame('Wielkopolskie', $company->getProvince());
        $this->assertSame('m. Poznań', $company->getDistrict());
        $this->assertSame('Poznań-Grunwald', $company->getCommune());
        $this->assertSame('Poznań', $company->getCity());
        $this->assertSame('60-201', $company->getPostCode());
        $this->assertSame('ul. Test-Wilcza', $company->getStreet());
    }

    protected function searchDataParameters(string $type = null) : array
    {
        //TOTEM.COM.PL i Enea
        $types = [
            'getByKrs' => [
                'factory' => SearchDataPramTypes::KRS,
                'value'   => '0000496427'
            ],
            'getByKrsy' => [
                'factory' => SearchDataPramTypes::KRSES,
                'value'   => ['0000496427', '0000012483']
            ],
            'getByNip' => [
                'factory' => SearchDataPramTypes::NIP,
                'value'   => '5561007611'
            ],
            'getByNipy' => [
                'factory' => SearchDataPramTypes::NIPS,
                'value'   => ['5561007611', '7770020640']
            ],
            'getByRegon' => [
                'factory' => SearchDataPramTypes::REGON,
                'value'   => '091187826'
            ],
            'getByRegony' => [
                'factory' => SearchDataPramTypes::REGONS_9,
                'value'   => ['091187826', '630139960']
            ],
            'getByRegony14zn' => [
                'factory' =>  SearchDataPramTypes::REGONS_14,
                'value'   => ['09118782600000', '63013996000000']
            ],
        ];
        return $type === null ? $types : $types[$type];
    }
}